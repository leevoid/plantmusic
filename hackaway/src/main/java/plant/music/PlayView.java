package plant.music;

import java.util.Random;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;
import java.util.function.Consumer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/*
 * How interactions with the plant are perceived via button clicking of light, water, and fertiliser.
 */
public class PlayView extends Application {

	// Initialising new object of plant
	Plant plant = new Plant();

	// Initialising a new instance of Random
	Random random = new Random();

	@FXML
	private Button light;

	@FXML
	private Button water;

	@FXML
	private Button fertiliser;

	@FXML
	private ImageView lightImage;

	@FXML
	private ImageView waterImage;

	@FXML
	private ImageView fertiliserImage;

	@FXML
	private ImageView seasonImage;

	@FXML
	private ImageView weatherImage;
	

	public PlayView(Plant plant) {
		this.plant = plant;
	}

	/*
	 * Initialising the three buttons as objects.
	 */
	@FXML
	public void initialise() {
		// Light button
		light.setOnAction(this::lightIncrease);

		// Water button
		water.setOnAction(this::waterIncrease);

		// Fertiliser button
		fertiliser.setOnAction(this::fertiliserIncrease);
	}

	/*
	 * When the user clicks on the light button, the amount the slider goes up by is
	 * a Random integer.
	 */
	private void lightIncrease(ActionEvent light) {
		plant.addLight(random.nextInt((25 - 5 + 1) + 5));
		showLight();
		hideLight();
	}

	/*
	 * When the user clicks on the water button, the amount the slider goes up by is
	 * a Random integer.
	 */
	private void waterIncrease(ActionEvent water) {
		plant.addWater(random.nextInt((25 - 5 + 1) + 5));
		showWater();
		hideWater();
	}

	/*
	 * When the user clicks on the fertiliser button, the amount the slider goes up
	 * by is a Random integer.
	 */
	private void fertiliserIncrease(ActionEvent fertiliser) {
		plant.addFertiliser(random.nextInt((25 - 5 + 1) + 5));
		showFertiliser();
		hideFertiliser();
	}

	/*
	 * When the user clicks on the water/sun/fertiliser button, Show the
	 * corresponding image for 3 seconds.
	 */
	private void showLight() {
		lightImage.setVisible(true);
	}
	
	private void hideLight() {
		lightImage.setVisible(false);
	}

	private void showWater() {
		waterImage.setVisible(true);
	}

	private void hideWater() {
		waterImage.setVisible(false);
	}

	private void showFertiliser() {
		fertiliserImage.setVisible(true);
	}

	private void hideFertiliser() {
		fertiliserImage.setVisible(false);
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		GridPane page = FXMLLoader.load(PlayView.class.getResource("view.fxml"));
		Scene scene = new Scene(page);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("MVC/Observer/fxml");
		primaryStage.show();
	}

	public void startView() {

	}

//DO NOT CHANGE ANYTHING BELOW THIS COMMENT
	/////////////////////////////////////////////////////////////////////////////////
	// Block for creating an instance variable for others to use.
	//
	// Make it a JavaFX singleton. Instance is set by the javafx "initialize" method
	private static volatile PlayView instance = null;

	@FXML
	void initialize() {
		instance = this;
	}

	/**
	 * This is a Singleton View constructed by the JavaaFX Thread and made available
	 * through this method.
	 * 
	 * @return the single object representing this view
	 */
	public static synchronized PlayView getInstance() {
		if (instance == null) {
			new Thread(() -> Application.launch(PlayView.class)).start();
			// Wait until the instance is ready since initialize has executed.
			while (instance == null) {// empty body
			}
		}

		return instance;
	}
	// End of special block
	/////////////////////////////////////////////////////////////////////////////////

}