package plant.music;

/*
 * How interactions with the plant are perceived via button clicking of 
 * light, water, and fertiliser.
 */
public class PlantController {
	
	GameLoop gameLoop;
	
	PlayView plantView;
	
	public PlantController (GameLoop gameModel, PlayView plantView) {
		
		this.gameLoop = gameModel;
		this.plantView = plantView;
	}

}
