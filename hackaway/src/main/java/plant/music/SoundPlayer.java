package plant.music;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URL;

/*
 * For now the SoundPlayer class plays a sound when the key a is pressed.
 * using the javax.sound.sampled for sound playback and java.swing for creating a Jframe
 * 
 * extra info :
 * JFrame is a top-level container that provides a window on the screen. 
 * A frame is actually a base window on which other components rely, namely 
 * the menu bar, panels, labels, text fields, buttons, etc. 
 * Almost every other Swing application starts with the JFrame window. 
 * 
 * ps : using JFrame was the only way to make it work
 * 
 * for now when you press A/a it plays the clip and Q/q to quit the program
 * */

public class SoundPlayer extends JFrame implements KeyListener {

    private Clip clip;
    /**
     * Constructs a SoundPlayer object, initializes the JFrame, and sets up the KeyListener.
     */
    public SoundPlayer() {
        super("Sound Player");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.addKeyListener(this);

        try {
            // Load the sound file
            URL soundFileUrl = getClass().getClassLoader().getResource("Good-Mode");
            if (soundFileUrl == null) {
                System.out.println("File not found");
                return;
            }

            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFileUrl);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            e.printStackTrace();
        }

        this.setSize(300, 200);
        this.setVisible(true);
    }

    /**
     * Plays the sound file
     */
    private void playSound() {
        if (clip != null) {
            clip.setFramePosition(0);  // Rewind to the beginning
            clip.start();              // Start playing the sound
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Unused
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == 'A' || e.getKeyChar() == 'a') {
            playSound();
        } else if (e.getKeyChar() == 'Q' || e.getKeyChar() == 'q') {
            System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // Unused
    }

    /**
     *
     * The main method that creates an instance of SoundPlayer in the Event Dispatch Thread.
     */
     
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new SoundPlayer());
    }
}
