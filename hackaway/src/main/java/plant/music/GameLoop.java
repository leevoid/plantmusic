package plant.music;

import java.util.Timer;

public class GameLoop {
	private Timer timer;
	private Plant plant;
	private Seasons seasons;
	private int curSeason;
	
	/** Set up variables for the game loop.
	 * 
	 */
	public GameLoop(Plant plant) {
		timer = new Timer();
		this.plant = plant;
		seasons = new Seasons();
		curSeason = 0;
	}
	
	public void playGame() {
		while (!plant.isDead()) {
			
		}
	}
	
	public void changeSeason() {
		curSeason++;
		seasons.changeSeason(curSeason%4);
	}

}
