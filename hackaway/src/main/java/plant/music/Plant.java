package plant.music;

public class Plant {
	private int waterLevel;
	private int lightLevel;
	private int fertiliserLevel;
	
	public Plant() {
		waterLevel = 30;
		lightLevel = 30;
		fertiliserLevel = 30;
	}
	
	public Plant(int waterLevel, int lightLevel, int fertiliserLevel) {
      this.waterLevel = waterLevel;
      this.lightLevel = lightLevel;
      this.fertiliserLevel = fertiliserLevel;
    }

  /** Checks if the plant is dead.
	 * @return whether or not the plant is dead
	 */
	public boolean isDead() {
		return waterLevel <= 0 || waterLevel >= 100 
				|| lightLevel <= 0 || lightLevel >= 100 
				|| fertiliserLevel <=0 || fertiliserLevel >= 100 ? true : false;
	}

	/** Returns the water level.
	 * @return the water level of the plant
	 */
	public int getWaterLevel() {
		return waterLevel;
	}

	/** Adds to the plant's current water level.
	 * @param waterLevel - the amount of water to be added to the plant's current water level
	 */
	public void addWater(int waterLevel) {
		if (this.waterLevel + waterLevel > 100) {
			this.waterLevel = 1000;
		}else {
			this.waterLevel += waterLevel;
		}
	}
	
	/** Removes from the plant's current water level.
	 * @param waterLevel - the amount of water to be removed from the plant's water level
	 */
	public void removeWater(int waterLevel) {
		if (this.waterLevel - waterLevel < 0) {
			this.waterLevel = 0;
		}else {
			this.waterLevel =- waterLevel;
		}
	}
	
	/** Returns the light level.
	 * @return the light level of the plant
	 */
	public int getLightLevel() {
		return lightLevel;
	}

	/** Adds to the plant's current light level.
	 * @param lightLevel - the amount of light to be added to the plant's current light level
	 */
	public void addLight(int lightLevel) {
		if (this.lightLevel + lightLevel > 100) {
			this.lightLevel = 1000;
		}else {
			this.lightLevel += lightLevel;
		}
	}
	
	/** Removes from the plant's current light level.
	 * @param lightLevel - the amount of light to be removed from the plant's light level
	 */
	public void removeLight(int lightLevel) {
		if (this.lightLevel - lightLevel < 0) {
			this.lightLevel = 0;
		}else {
			this.lightLevel =- lightLevel;
		}
	}
	
	/** Returns the fertiliser level.
	 * @return the fertiliser level of the plant
	 */
	public int getFertiliserLevel() {
		return fertiliserLevel;
	}

	/** Adds to the plant's current fertiliser level.
	 * @param fertiliserLevel - the amount of fertiliser to be added to the plant's current fertiliser level
	 */
	public void addFertiliser(int fertiliserLevel) {
		if (this.fertiliserLevel + fertiliserLevel > 100) {
			this.fertiliserLevel = 100;
		}else {
			this.fertiliserLevel += fertiliserLevel;
		}
	}
	
	/** Removes from the plant's current fertiliser level.
	 * @param fertiliserLevel - the amount of fertiliser to be removed from the plant's fertiliser level
	 */
	public void removeFertiliser(int fertiliserLevel) {
		if (this.fertiliserLevel - fertiliserLevel < 0) {
			this.fertiliserLevel = 0;
		}else {
			this.fertiliserLevel =- fertiliserLevel;
		}
	}

}
