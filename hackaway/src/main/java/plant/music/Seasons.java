package plant.music;

public class Seasons {
	int waterDepletionFactor;
	int lightDepletionFactor;
	int fertiliserDepletionFactor;
	
	/** Setting up initial season variables.
	 * 
	 */
	public Seasons() {
		waterDepletionFactor = 5;
		lightDepletionFactor = 5;
		fertiliserDepletionFactor = 5;
	}
	
	
	/** Returns the water Depletion Factor.
	 * @return - the water depletion factor
	 */
	public int getWaterDepletionFactor() {
		return waterDepletionFactor;
	}
	
	/** Returns the Light Depletion Factor.
	 * @return - the light depletion factor
	 */
	public int getLightDepletionFactor() {
		return lightDepletionFactor;
	}
	
	/** Returns the fertiliser Depletion Factor.
	 * @return - the fertiliser depletion factor
	 */
	public int getFertiliserDepletionFactor() {
		return fertiliserDepletionFactor;
	}
	
	/** Updates the depletion factors depending on the season.
	 * @param season - the number corresponding to the current season
	 */
	public void changeSeason(int season) {
		switch (season) {
		case 0: //winter
			waterDepletionFactor = 5;
			lightDepletionFactor = 5;
			fertiliserDepletionFactor = 5;
			break;
		case 1: //spring
			waterDepletionFactor = 10;
			lightDepletionFactor = 15;
			fertiliserDepletionFactor = 30;
			break;
		case 2: //summer
			waterDepletionFactor = 30;
			lightDepletionFactor = 5;
			fertiliserDepletionFactor = 15;
			break;
		case 3: //spring
			waterDepletionFactor = 10;
			lightDepletionFactor = 10;
			fertiliserDepletionFactor = 10;
			break;
		}
	}
	

}
