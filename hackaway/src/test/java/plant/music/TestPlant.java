package plant.music;

import static org.junit.jupiter.api.Assertions.*;

//import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestPlant {
	Plant plant;
	
//	@BeforeAll
//	void setUp() {
//		plant = new Plant();
//	}

	@Test
	void testPlantNotDead() {
		Plant plant = new Plant();
		assertFalse(plant.isDead());
	}

	@Test
	void testPlantIsDeadWater() {
		Plant plant = new Plant();
		plant.removeWater(30);
		assertTrue(plant.isDead());
	}
	
	@Test
	void testPlantIsDeadLight() {
		Plant plant = new Plant();
		plant.removeLight(30);
		assertTrue(plant.isDead());
	}
	
	@Test
	void testPlantIsDeadFertiliser() {
		Plant plant = new Plant();
		plant.removeFertiliser(30);
		assertTrue(plant.isDead());
	}


}
